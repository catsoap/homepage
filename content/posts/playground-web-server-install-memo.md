---
title: "Playground Web Server Install Memo"
date: 2020-11-15T20:01:34+01:00
---

Confined, again.  
This time I took a cheap VPS to play, this humble weblog is its first incarnation.

## Base installation

I want to keep things simple, as my VPS will be mainly for experimenting,
my goal is to have the faster workflow from my local to prod environment.

I just had to install 2 programs: [Caddy](https://caddyserver.com/) and [docker](https://www.docker.com/).

## Personal account

I just added myself to `sudo` and `docker`, and then copied my SSH key with `ssh-copy-id`.  
The only important thing here is to remember to test a connection, before we disable password login.

## Deployer account

The deployer account have to belong to `sudo` and `docker` groups.  
We must give it a password, just for the sake of authorizing its SSH key on the machine.

```sh
sudo useradd --create-home -s /bin/bash deploy
sudo adduser deploy sudo
sudo adduser deploy docker
passwd deploy
sudo su deploy
ssh-keygen -o -b 4096
ssh-copy-id -i ~/.ssh/id_rsa.pub localhost
```

## Base security

Disable the possibility to login with a password,
the line to uncomment and change is `PasswordAuthentication no`  
Then let's remember to try to login in another terminal, before quitting the server
and figure out that we forgot some detail.

```sh
sudo vim /etc/ssh/ssh_config
sudo systemctl restart sshd
```

## Caddy

I'll be publishing my static experiments in `/var/www`, so I change the ownership of
this dir so that the deployer can write there.

```sh
sudo mkdir /var/www
sudo chown deploy.deploy /var/www
```

My Caddyfile looks like this:

```sh
sudo cat /etc/caddy/Caddyfile
sjehan.net

root * /var/www/sjehan.net
file_server
```

## Gitlab CI

Now I can have gitlab ci automatically deploy when I push on this site repo.

For the CI part, we need to add the deployer account SSH key in the repository environment variables.

```sh
sudo su deploy
cat /home/deploy/.ssh/id_rsa
```

My gitlab ci file looks like this:

```yaml
stages:
    - build
    - deploy

build:
    image: registry.gitlab.com/capedev-labs/docker/hugo-with-git/0-55-6:latest
    stage: build
    script:
        - git submodule update --init
        - hugo
    artifacts:
        paths:
            - public

deploy:
    stage: deploy
    before_script:
        - "which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )"
        - eval $(ssh-agent -s)
        - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null
        - mkdir -p ~/.ssh
        - chmod 700 ~/.ssh
        - ssh-keyscan $PROD_SERVER_IP >> ~/.ssh/known_hosts
        - chmod 644 ~/.ssh/known_hosts
    script:
        - ssh deploy@$PROD_SERVER_IP "rm -rf /var/www/sjehan.net/*"
        - scp -rp public/* deploy@$PROD_SERVER_IP:/var/www/sjehan.net
```

That's it!
